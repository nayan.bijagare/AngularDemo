var app = angular.module("filterModule", []);
app.controller("filterCntr", filterCntr);

function filterCntr() {
    this.array = [{
        "name": "Nayan",
        "age": "20",
    },
        {
            "name": "Amit",
            "age": "18",
        },
        {
            "name": "Tushar",
            "age": "24",
        },
        {
            "name": "Vivek",
            "age": "10",
        },
        {
            "name": "Sunil",
            "age": "26",
        },
        {
            "name": "Saurav",
            "age": "28",
        },
        {
            "name": "Sachin",
            "age": "24",
        },

    ]
}

app.filter('myFormat', function() {
    return function(x) {
        var i, c, txt = "";
        for (i = 0; i < x.length; i++) {
            c = x[i];
            if (i % 2 == 0) {
                c = c.toUpperCase();
            }
            txt += c;
        }
        return txt;
    };
});