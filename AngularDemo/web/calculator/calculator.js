var app = angular.module("calciModule",[]);

app.controller("calCtr",calCtr);

function calCtr($scope) {
    $scope.operate = function(operator) {

        if ( $scope.firstNumber == null || $scope.secondNumber == null) {
            alert("please enter a number ");
        }

        else {
            $scope.operation = operator;
            switch (operator) {
                case "+" :
                    $scope.result = parseInt($scope.firstNumber) + parseInt($scope.secondNumber);
                    break;
                case "-" :
                    $scope.result = parseInt($scope.firstNumber) - parseInt($scope.secondNumber);
                    break;
                case "*" :
                    $scope.result = parseInt($scope.firstNumber) * parseInt($scope.secondNumber);
                    break;
                case "/" :
                    $scope.result = parseInt($scope.firstNumber) / parseInt($scope.secondNumber);
                    break;
            }
        }
    }
}
