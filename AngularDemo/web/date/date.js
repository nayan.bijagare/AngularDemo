var app = angular.module("dateModule", []);
app.controller("dateController", dateController);

function dateController($scope){
    $scope.date = new Date().toTimeString();
    $scope.updateTime = function () {
        console.log("I am called")
        $scope.date = new Date().toTimeString();
    }
}