var app = angular.module("taskModule", []);
app.controller("taskContr", taskContr);

function taskContr() {
    this.textArray = [];
    this.isEdited = false;
    this.add = function (index) {
        if (this.name == null || this.name == '') {
            alert("Please enter text")
        } else {
            this.textArray.push({"name": this.name});
        }
    }

    this.delete = function (taskIndex) {
        this.textArray.splice(taskIndex, 1);
    }

    this.edit = function (taskIndex) {
        this.isEdited = !this.isEdited;
    }

    this.deleteAll = function() {
        this.textArray = [];
    }
}