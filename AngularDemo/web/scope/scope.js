var app = angular.module("scopeModule", []);

app.controller("ctr1", module1);
app.controller("ctr2", module2);

function module1() {
   console.log("module1");
    this.name = "scope1";

    this.array = [
        {"name":"nayan",
         "age":"20"
        },
        {"name":"manoj",
            "age":"25"
        }
    ];

    this.array.push({"name":"Amit",
        "age":"26"
    });

    this.array.splice(0,1);
}


function module2() {
    console.log("module2");
    this.name = "scope2";
}
